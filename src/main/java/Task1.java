import java.math.BigInteger;

public class Task1 {

    // Method using StringBuilder
    public static String addStringBuilder(String num1, String num2) {
        StringBuilder sb = new StringBuilder();
        int carry = 0;

        int i = num1.length() - 1;
        int j = num2.length() - 1;

        while (i >= 0 || j >= 0) {
            int a = i >= 0 ? num1.charAt(i) - '0' : 0;
            int b = j >= 0 ? num2.charAt(j) - '0' : 0;
            int sum = a + b + carry;

            sb.append(sum % 10);
            carry = sum / 10;

            i--;
            j--;
        }

        if (carry > 0) {
            sb.append(carry);
        }

        return sb.reverse().toString();
    }

    // Method using BigInteger
    public static String addBigInteger(String num1, String num2) {
        BigInteger bigNum1 = new BigInteger(num1);
        BigInteger bigNum2 = new BigInteger(num2);
        BigInteger result = bigNum1.add(bigNum2);
        return result.toString();
    }

    public static boolean areCoprime(int a, int b, int c) {
        return gcd(a, b) == 1 && gcd(a, c) == 1 && gcd(b, c) == 1;
    }

    private static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);
    }
}
