public class Task3 {
    // Method that checks if a number is an Armstrong number
    public static boolean isArmstrong(int num) {
        int n = String.valueOf(num).length();
        int sum = 0;
        int temp = num;

        while (temp != 0) {
            int digit = temp % 10;
            sum += (int) Math.pow(digit, n);
            temp /= 10;
        }

        return sum == num;
    }

    // Method that finds all Armstrong numbers from 1 to k
    public static void findArmstrong(int k) {
        for (int i = 1; i <= k; i++) {
            if (isArmstrong(i)) {
                System.out.print(i + " ");
            }
        }
    }

    public static void main(String[] args) {
        findArmstrong(10000);
    }
}
