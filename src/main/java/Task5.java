import java.util.Arrays;

public class Task5 {
    // Method that checks if the sum of points on the front faces of the dice in any three neighboring cells is 10
    public static boolean checkCode(int[] cells) {
        for (int i = 0; i < cells.length - 2; i++) {
            if (cells[i] + cells[i + 1] + cells[i + 2] == 10) {
                return true;
            }
        }
        return false;
    }

    // Method that generates all possible combinations of dice rolls and inserts them into the lock
    public static void solveCode(int[] cells, int index) {
        if (index == cells.length) {
            if (checkCode(cells)) {
                System.out.println("Code found: " + Arrays.toString(cells));
            }
            return;
        }

        for (int i = 1; i <= 6; i++) {
            cells[index] = i;
            solveCode(cells, index + 1);
        }
    }

    public static void main(String[] args) {
        int[] cells = new int[10];
        // Assume that two dice are already inserted into cells 1 and 2
        cells[0] = 2;
        cells[1] = 4;

        solveCode(cells, 2);
    }
}
